const getPathname = (url) => {
    if(url[0] === '/') {
        return url;
    } else {
        const urlObj = new URL(url);
        return urlObj.pathname;
    }
}

module.exports = { getPathname };